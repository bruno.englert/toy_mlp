# Toy example of a neural net

## What it does?

1. Loads MNIST data. (http://yann.lecun.com/exdb/mnist/)
2. Teaches an MLP to predict the handwritten digits. (https://en.wikipedia.org/wiki/Multilayer_perceptron)
3. After teaching, it predicts a few digits.

## Requirements

* python3
* To install necessary packages, run: `pip3 install -r requirements.txt`

## How to run?
`python3 main.py`