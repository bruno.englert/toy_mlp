import numpy as np


# Sigmoid function
def sigmoid(x):
    return 1 / (1 + np.exp(-x))


# Sigmoid derivative function
def de_sigmoid(x):
    return np.exp(-x) / ((1 + np.exp(-x)) ** 2)


# ReLU function
def relu(x):
    return np.maximum(x, 0.0)


# ReLU derivative function
def de_relu(x):
    res = np.zeros(shape=x.shape, dtype=np.float)
    res[x > 0] = 1.0
    return res